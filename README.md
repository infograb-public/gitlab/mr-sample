# MR Sample

'Merge Request로 협업하기' 예제 프로젝트 입니다.

영상에서는 각 사용자별로 MR에서의 역할을 수행하고 협업하는 내용을 다룹니다.

Merge Request는 왜 사용하고 GitLab이 제시하는 협업 방식은 무엇일까요? 예제를 통해 MR에서 각 사용자들의 역할을 알아보고 이슈에서 MR을 직접 만들어 봅니다. 그리고 커밋과 리뷰를 통해 MR로 협업하고 Merge 후 파이프라인을 통해 Artifact를 확인해 봅니다. 끝으로 Merge Request의 장점도 알려 드립니다.

- [1. Merge Request 왜 사용해야 할까?(2:08)](https://www.youtube.com/watch?v=OZS6vqTN4Hg&list=PLJcWSUQkvYYRLVrrZKgYeIeluG5dlcjqj&index=1)
- [2. Merge Request 만들기(10:45)](https://www.youtube.com/watch?v=pOBzXtI4sHQ&list=PLJcWSUQkvYYRLVrrZKgYeIeluG5dlcjqj&index=2)
- [3. Merge Request로 개발 협업하기(9:57)](https://www.youtube.com/watch?v=Ht8Spfg_4lQ&list=PLJcWSUQkvYYRLVrrZKgYeIeluG5dlcjqj&index=3)
- [4. Merge Request의 장점(3:13)](https://www.youtube.com/watch?v=UlO-fMSQCfE&list=PLJcWSUQkvYYRLVrrZKgYeIeluG5dlcjqj&index=4)


## 프로젝트 구성

React-Native로 로그인 화면을 구현한 예제 프로젝트 입니다.
issue와 MR 템플릿이 포함되어 있고, 유튜브 영상에서 진행한 issue와 MR 예제가 포함되어 있습니다.

## 파이프라인 구성

`build`, `test`, `release` 단계가 포함되어 있습니다.

## Figma

예제에서 사용한 [피그마 페이지 링크](https://www.figma.com/file/2hHGbjOixrUkMcZJiXmZTI/%EB%A1%9C%EA%B7%B8%EC%9D%B8-%EB%B7%B0?node-id=0%3A1) 입니다.
