import React, {useState} from 'react';
import {StatusBar, View, Text, TouchableOpacity, Image} from 'react-native';
import styled from 'styled-components/native';

const Container = styled.SafeAreaView`
  margin: 16px;
`;

const Icon = styled.Image`
  width: 13px;
  height: 12px;
`;

const Title = styled.Text`
  font-size: 36px;
  margin: 32px 0;
`;

const Input = styled.TextInput`
  height: 52px;
  margin-bottom: 16px;
  padding: 17px;
  border: 2px solid #000;
`;

const LoginButton = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 52px;
  background: #000;
  border-radius: 6px;
`;

const LoginText = styled.Text`
  color: #fff;
`;

const PolicyText = styled.Text`
  padding: 10px 0;
  font-size: 13px;
`;

const App: React.FC = () => {
  const [id, setId] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Container>
      <StatusBar barStyle="light-content" />
      <TouchableOpacity>
        <Icon source={require('./img/back.png')} />
      </TouchableOpacity>
      <Title>로그인</Title>
      <View>
        <Input placeholder="아이디" value={id} onChangeText={setId} />
        <Input
          placeholder="비밀번호"
          secureTextEntry
          value={password}
          onChangeText={setPassword}
        />
        <LoginButton>
          <LoginText>로그인</LoginText>
        </LoginButton>
        <TouchableOpacity>
          <PolicyText>이용약관 및 개인정보처리방침</PolicyText>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

export default App;
